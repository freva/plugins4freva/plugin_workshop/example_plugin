"""
psi-main.py

This script loads for each area, the arrays of daily precipitation, 
finds the percentile for each grid point, calculate PSI for 
every day a NetCDF file.
It also produces two plots: psi and the selected box area.

Developers:
    Alberto Caldas-Alvarez (alberto.caldas-alvarez@kit.edu) Module A - ClimXtreme
    Etor E. Lucio-Eceiza (lucio-eceiza@dkrz.de) Module D/Software - ClimXtreme
version: v2.2.0
date: 2024.05.08
"""

import os, sys
from utils.utils import DataHandler

import numpy as np
import xarray as xr
from datetime import datetime
import cftime
import matplotlib

matplotlib.use("agg")
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from shapely.geometry.polygon import LinearRing
import json

import dask

dask.config.set(scheduler="threads", num_workers=4)
dask.config.set(**{"array.slicing.split_large_chunks": False})


class PSI(DataHandler):
    """
    class to calculate the Precipitation Severity Index (PSI, by ClimXtreme - Module A)
    """

    def __init__(self, configuration_filepath):
        """
        Initializes an instance of the class PSI.

        Args:
            configuration_filepath (str): The path to the configuration file.

        Raises:
            ValueError: If the configuration file path is not provided.

        Returns:
            None
        """
        try:
            with open(configuration_filepath) as f:
                self.config_dict = json.load(f)
        except IndexError:
            raise ValueError("Usage: {} path_to_json_file.json".format(sys.argv[0]))
        self.config_dict["percentile_threshold"] = False # it takes absolute precip values as threshold
        self.config_dict["selmon"] = "all" # select all months
        self.config_dict["area_weight"] = "cos" # area weighting according to cos(lat)
        self.config_dict["spatial_psi"] = False
        if not self.config_dict["ensemble"]:
            self.config_dict["ensemble"] = ["no-member"]
        super().__init__(self.config_dict)

    def get_configuration(self):
        """
        Returns the configuration dictionary.

        :return: The configuration dictionary.
        """
        return self.config_dict

    def output_filename(self, ensemble_member):
        """
        Output filename for the given ensemble member.

        Args:
            ensemble_member (str): The ensemble member for which the output filename will be generated.

        Returns:
            str: The output filename.
        """
        output_name = (
            f'{self.config_dict["outputdir"]}/{ensemble_member}/{ensemble_member}'
        )
        return output_name

    def psi_calculation(self, da):
        """
        Calculates the PSI (Precipitation Severity Index) for a given dataset.

        Parameters:
            da (DataArray): The input datarray containing the precipitation data.
        Returns:

            psi_output (ndarray): PSI values for each time step.
            affec_area (ndarray): affected area based on the PSI values.
            max_gp_prec (ndarray): maximum generalised pareto precipitation.
            fldsum_prec (ndarray): fieldsum precipitation.
            spatial_psi (ndarray): spatial intensity of precipitation.
        Note:
            - The function requires the following configuration parameters to be set in the config_dict attribute:
                - percentile_threshold: A boolean indicating whether to use percentile threshold or a fixed threshold.
                - threshold: The threshold value to use for calculating the PSI.
                - persistence: The number of days to consider for calculating the spatial intensity.
        """
        print(f"\nPSI calculation")
        time_size = da.time.size
        da_shape = da.shape
        da = da.values
        max_gp_prec = np.nanmax(da, axis=(1, 2))
        fldsum_prec = np.nansum(da, axis=(1, 2))
        if self.config_dict["percentile_threshold"]:
            thres_spat_distr = np.percentile(
                da, int(self.config_dict["threshold"]), axis=0, method="higher"
            )
            thres_spat_distr[thres_spat_distr == 0] = np.NaN
        else:
            thres_spat_distr = np.full(da_shape, float(self.config_dict["threshold"]))

        precipitation_ratio = np.true_divide(da, thres_spat_distr)
        del da
        precipitation_ratio_flat = precipitation_ratio.reshape(-1)
        precipitation_ratio_flat[precipitation_ratio_flat <= 1] = 0
        precipitation_ratio = precipitation_ratio_flat.reshape(
            precipitation_ratio.shape
        )

        affected_area = np.where(precipitation_ratio > 1, 1, 0)
        affec_area = (
            100
            * np.nansum(affected_area, axis=(1, 2))
            / np.count_nonzero(~np.isnan(affected_area), axis=(1, 2))
        )
        psi_output = np.full(time_size, np.NaN)
        if self.config_dict["spatial_psi"]:
            spatial_psi = np.full(da_shape, np.NaN)
        else:
            spatial_psi = np.array(0)

        precipitation_ratio_accumulated_days = np.full(
            (int(self.config_dict["persistence"]) + 1, da_shape[1], da_shape[2]), np.NaN
        )
        for i in range(0 + int(self.config_dict["persistence"]), time_size):
            for n, t in enumerate(
                range(i, i - int(self.config_dict["persistence"]) - 1, -1)
            ):
                affected_area_dummy = np.prod(affected_area[i:t:-1], axis=0)
                precipitation_ratio_accumulated_days[n] = (
                    precipitation_ratio[t] * affected_area_dummy
                )
            if self.config_dict["spatial_psi"]:
                spatial_psi[i] = precipitation_ratio_accumulated_days.sum(axis=0)
            psi_output[i] = np.nansum(precipitation_ratio_accumulated_days)

        return psi_output, affec_area, max_gp_prec, fldsum_prec, spatial_psi

    def plot_temporal(
        self, time_coord, psi_output, output_name, num_events=10, step_ticks=5
    ):
        """
        Plots the PSI (Precipitation Sensitivity Index).

        Parameters:
            time_coord (coordinate): The time coordinate of the precipitation datarray.
            psi_output (ndarray): The numpy darray containing the psi values.
            output_name (str): The name of the output.
            num_events (int, optional): The maximum number of events to plot. Defaults to 10.
            step_ticks (int, optional): Number of timesteps in xticks. Defaults to 5.

        Returns:
            None
        """
        print(f"\ntemporal plot of PSI")
        len_time = len(time_coord)
        num_events = min(len_time, num_events)
        nan_count = sum(np.isnan(psi_output))
        max_events = psi_output.argsort()[::-1][nan_count : num_events + nan_count]
        psi_max = np.sort(psi_output[max_events[:]])[0]
        step_size = len_time // step_ticks
        format = self._get_time_freq(time_coord)
        custom_xticks = [
            np.datetime_as_string(np.datetime64(date), unit=format)
            for date in time_coord[::step_size].values
        ]
        custom_indices = np.arange(0, len(time_coord), step_size)
        fig = plt.figure(figsize=(18.5, 10.5))
        plt.rcParams.update(
            {"font.size": 24, "axes.spines.top": False, "axes.spines.right": False}
        )

        plt.ylabel("PSI")

        if self.config_dict["percentile_threshold"]:
            perc = "perc"
        else:
            perc = "fixd-thres [mm/d]"
        plt.title(
            "PSI for daily prec. over %s [%s %s, %s acc. days]"
            % (
                os.path.basename(output_name),
                self.config_dict["threshold"],
                perc,
                self.config_dict["persistence"],
            ),
            size=22,
        )
        plt.plot(psi_output[:])
        plt.xticks(custom_indices, custom_xticks)
        plt.scatter(
            max_events,
            psi_output[max_events[:]],
            s=200,
            facecolors="none",
            linewidth=3,
            edgecolors="orange",
        )
        plt.hlines(
            psi_max,
            0,
            len_time,
            colors="gray",
            linestyles="dashed",
        )
        plt.text(
            0,
            psi_max * 1.01,
            "thres. %i events: %6.1f" % (num_events, psi_max),
            fontsize=22,
        )
        plt.savefig(f"{output_name}-psi.png", dpi=100)

    def plot_spatial_box(self, output_name):
        """
        Plot a spatial box on a map and save it as an image file.

        Args:
            output_name (str): The name of the output image file.

        Returns:
        print(f"\ntemporal plot of PSI")
            None
        """
        print(f"\nplotting spatial area of analysis")
        fig, ax = plt.subplots(
            figsize=(8, 8), subplot_kw=dict(projection=ccrs.PlateCarree())
        )

        ocean = cfeature.NaturalEarthFeature(
            "physical", "ocean", "50m", facecolor=cfeature.COLORS["water"]
        )
        lakes = cfeature.NaturalEarthFeature(
            "physical", "lakes", "50m", facecolor=cfeature.COLORS["water"]
        )
        land = cfeature.NaturalEarthFeature(
            "physical", "land", "50m", facecolor=cfeature.COLORS["land"]
        )
        states = cfeature.NaturalEarthFeature(
            category="cultural",
            facecolor="none",
            name="admin_0_boundary_lines_land",
            scale="50m",
        )

        ax.add_feature(ocean)
        ax.add_feature(land, edgecolor="black")
        ax.add_feature(lakes)
        ax.add_feature(states, edgecolor="gray", alpha=0.5)

        # adding selected area:
        if self.lon_max:
            dlon = (self.lon_max - self.lon_min) * 1.1
            dlat = (self.lat_max - self.lat_min) * 1.1

            lonA = self.lon_min - dlon
            lonB = self.lon_max + dlon
            latA = self.lat_min - dlat
            latB = self.lat_max + dlat
            ax.set_extent([lonA, lonB, latA, latB], crs=ccrs.PlateCarree())
            lonm = [self.lon_min, self.lon_min, self.lon_max, self.lon_max]
            latm = [self.lat_min, self.lat_max, self.lat_max, self.lat_min]
            ring = LinearRing(list(zip(lonm, latm)))
            ax.add_geometries(
                [ring],
                ccrs.PlateCarree(),
                facecolor="none",
                edgecolor="black",
                linewidth=2,
            )
            plt.title(
                "sel. area: [%3.2f : %3.2f , %3.2f : %3.2f] (lon,lat)"
                % (self.lon_min, self.lon_max, self.lat_min, self.lat_max),
                size=15,
            )
        fig.savefig(f"{output_name}-selbox.png", dpi=100)

    def save_netcdf(
        self,
        da,
        psi_output,
        affec_area,
        max_gp_prec,
        fldsum_prec,
        spatial_psi,
        output_name,
    ):
        """
        Generates a precipitation severity index (PSI) dataset based on the input parameters.

        Args:
            da (xr.Datarray): The input template datarray.
            psi_output (ndarray): The PSI output values.
            affected_area (ndarray): The affected area by precipitation.
            max_gp_prec (ndarray): maximum generalised pareto precipitation.
            fldsum_prec (ndarray): fieldsum precipitation.
            spatial_psi (ndarray): The spatial PSI values.

        Returns:
            xr.Dataset: The input ndarrays as dataset.
        """
        print(f"\nsave in netcdf")
        if self.config_dict["spatial_psi"]:
            psi_ds = xr.Dataset(coords=da.coords)
        else:
            psi_ds = xr.Dataset(coords={"time": da["time"]})
        miss = float(1.0e20)
        psi_ds["psi"] = (["time"], psi_output.astype("float32"))
        psi_ds["psi"].attrs = {"units": "None", "_FillValue": miss}
        psi_ds = psi_ds.fillna(miss)
        psi_ds = psi_ds.where(psi_ds["psi"] != miss, drop=True)
        if hasattr(self.ds, "time_bnds"):
            psi_ds["time_bnds"] = self.ds["time_bnds"]

        if isinstance(psi_ds["time"].values[0], np.datetime64):
            ini_date = np.datetime_as_string(psi_ds["time"].values[0])
            end_date = np.datetime_as_string(psi_ds["time"].values[-1])  
        elif isinstance(psi_ds["time"].values[0], cftime.datetime):
            ini_date = psi_ds["time"].values[0].strftime("%Y-%m-%d")
            end_date = psi_ds["time"].values[-1].strftime("%Y-%m-%d")
        else:
            raise TypeError("Unsupported time format")

        if self.config_dict["percentile_threshold"]:
            perc = "per"
        else:
            perc = "thres[mm/d]"
        psi_ds.attrs = {
            "project": "ClimXtreme",
            "institution": "KIT",
            "title": "Precipitation Severity Index",
            "history": f"created from {self.files}",
            "creation_date": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "calc_parameters": f"{perc}: {self.config_dict['threshold']}, acc.day: {self.config_dict['persistence']}, area: {self.config_dict['latlon']}, time: {ini_date} - {end_date}, area_weight: {self.config_dict['area_weight']}",
            "contact": "Alberto Caldas Alvarez, alberto.caldas-alvarez@kit.edu",
        }
        psi_ds.to_netcdf(output_name)


def Main():
    psi_analysis = PSI(sys.argv[1])
    print(f"\nloading parameter configuration")
    config_dict = psi_analysis.get_configuration()
    
    for ensemble_member in config_dict["ensemble"]:
        print(f"\nProcessing {ensemble_member =}")
        pathdir_output = f'{config_dict["outputdir"]}/{ensemble_member}'
        if not os.path.exists(pathdir_output):
            os.mkdir(pathdir_output)
        da = psi_analysis.open_files(ensemble_member)
        output_name = psi_analysis.output_filename(ensemble_member)
        psi_output, affec_area, max_gp_prec, fldsum_prec, spatial_psi = (
            psi_analysis.psi_calculation(da)
        )
        psi_analysis.save_netcdf(
            da,
            psi_output,
            affec_area,
            max_gp_prec,
            fldsum_prec,
            spatial_psi,
            f"{output_name}-psi.nc",
        )
        if config_dict["spatial_psi"]:
            psi_analysis.plot_spatial_psi(
                da, psi_output, spatial_psi, output_name, num_events=10
            )
            del spatial_psi
        psi_analysis.plot_spatial_box(output_name)
        psi_analysis.plot_temporal(da["time"], psi_output, output_name, num_events=10)


if __name__ == "__main__":
    Main()
