from shutil import rmtree
import os, sys, re
import warnings
from datetime import datetime
from pathlib import Path
from typing import cast, Collection, Union

from evaluation_system.api import plugin
from evaluation_system.misc import logger
from evaluation_system.api.parameters import (
    ParameterDictionary as ParamDict,
    Bool,
    String,
    Integer,
    File,
    SolrField,
    SelectField,
    InputDirectory,
    Directory,
    CacheDirectory,
)

import freva


class ExamplePlugin(plugin.PluginAbstract):
    __category__ = "example"
    __tags__ = ["precipitation", "extremes", "indices", "freva-dev"]
    tool_developer = {
        "name": "Freva Team",
        "email": "freva@dkrz.de",
    }
    __short_description__ = "Calculate Precipitation Indices."
    __long_description__ = """Example Tool that calculates various precipitation indices.
    
    A tool that calculates either the Precipitation Severity Index (PSI), or the Consecutive Dry and Wet Days (CDD, CWD). 
    
        - It ingests daily precipitation data and outputs a NetCDF file with the corresponding info.
        - The input can be either browsed from the database or directly introduced as a folderpath.
        - It offers the possibility to select a certain period of time and lat,lon box for the analysis.
        - It also plots the spatial or temporal indices.

    Based on CDO (climate data operators) and Alvarez et al. (2023).
    """
    __version__ = (1, 0, 0)
    __parameters__ = ParamDict(
        ##############################################
        # 1. Input
        ##############################################
        InputDirectory(
            name="inputdir",
            default=None,
            help=("Folder path of data to be analyse instead of databrowser"),
        ),
        SolrField(
            name="project",
            facet="project",
            predefined_facets={
                "time_frequency": ["day"],
                "variable": ["pr"],
            },            
            help=("Choose project, e.g. reanalysis, cmip5, baseline 1, baseline 0"),
        ),
        SolrField(
            name="product",
            facet="product",
            predefined_facets={
                "time_frequency": ["day"],
                "variable": ["pr"],
            },            
            help=("Choose product, e.g. reanalysis, output"),
        ),
        SolrField(
            name="institute",
            facet="institute",
            predefined_facets={
                "time_frequency": ["day"],
                "variable": ["pr"],
            },            
            help=("Choose institute of experiment, e.g. MPI-M"),
        ),
        SolrField(
            name="model",
            facet="model",
            predefined_facets={
                "time_frequency": ["day"],
                "variable": ["pr"],
            },            
            help=("Choose model of experiment, e.g. MPI-ESM-LR"),
        ),
        SolrField(
            name="experiment",
            facet="experiment",
            predefined_facets={
                "time_frequency": ["day"],
                "variable": ["pr"],
            },
            help=("Choose experiment name, e.g. decadal1971"),
        ),
        SolrField(
            name="ensemble",
            facet="ensemble",
            multiple=True,
            default=None,
            help=(
                "Choose ensemble member - multi selection possible (comma separated), use * for all members"
            ),
        ),
        ##############################################
        # 2. Output
        ##############################################
        Directory(
            name="outputdir",
            default="$USER_OUTPUT_DIR/$SYSTEM_DATETIME",
            mandatory=True,
            help=("The default output directory for the freva-wrk instance"),
        ),
        CacheDirectory(
            name="cache",
            default="$USER_CACHE_DIR",
            mandatory=True,
        ),
        Bool(
            name="cacheclear",
            default=True,
            help=("Option switch to NOT clear the cache"),
        ),
        ##############################################
        # 3. Plugin specific settings
        ##############################################
        SelectField(
            name="select_index",
            default="psi",
            options={"psi": "psi", "cdd_cwd": "cdd_cwd"},
            help=(
                "Option to apply psi (Precipitation Severity Index) or cdd (Consecutive Dry Days) + cwd (Consecutive Wet Days)."
            ),
        ),        
        String(
            name="seldate",
            default="None",
            help=(
                "Option to choose timerange start,end via YYYY-MM-DD,YYYY-MM-DD. By default takes the whole period."
            ),
        ),
        String(
            name="latlon",
            default="None",
            help=(
                "Option to choose an area in file. Usage startlat,endlat,startlon,endlon -> For europe 30,80,-20,30. By the default takes the whole area."
            ),
        ),
        Integer(
            name="threshold",
            default=1,
            help=(
                "The daily grid-point precipitation threshold in mm/day."
            ),
        ),
        Integer(
            name="persistence",
            default=2,
            help=(
                "The number of accumulated (psi) or consecutive (cdd, cwd) days to be taken for the calculation."
            ),
        ),
        Bool(
            name="link2database",
            default=False,
            help=("Set to 'True' to link the output to the data-browser as user data."),
        ),
        
    )

    # -----------

    def dateparser(self, text: str) -> datetime:
        """parse string into datetime

        Parameters
        ----------
        date: str
            a date in several formats

        Returns
        -------
        date: datetime
            date in datetime format
        """
        for fmt in ("%Y", "%Y-%m", "%Y-%m-%d", "%Y-%m-%dT%H:%M"):
            try:
                return datetime.strptime(text, fmt)
            except ValueError:
                pass

    def parse_date(self, config_dict: dict[str, Union[str, list[str], bool]]) -> None:
        """parse date range for databrowse in chronological order

        Parameters
        ----------
        config_dict: dict
            The plugin configuration

        Returns
        -------
        config_dict: dict
            correctly parsed time:value, if start or end is empty, it fills it

        Raises
        ------
        TypeError, ValueError:
            If date format not correct
        """
        try:
            if not config_dict["seldate"] or config_dict["seldate"] == "None":
                config_dict["seldate"] = None
                return
            else:
                start, end = config_dict["seldate"].split(",")
        except TypeError:
            raise TypeError(
                "date format is not correct in %s" % (config_dict["seldate"])
            )
        except ValueError:
            raise ValueError(
                "date format is not correct: %s" % (config_dict["seldate"])
            )

        if not start:
            start = "0001-01-01T00:00"
        if not end:
            end = "9999-12-31T23:59"
        self.dateparser(start)
        self.dateparser(end)
        try:
            if start > end:
                tmp = start
                start = end
                end = tmp
            time = f"{start}to{end}"
        except TypeError:
            raise ValueError(
                "date format is not correct: %s" % (config_dict["seldate"])
            )
        config_dict["seldate"] = time

        return

    def find_files(self, config_dict: dict[str, Union[str, list[str], bool]]) -> None:
        """Find all relevant files as a dictionary

        Parameters
        ----------
        config_dict: dict
            The plugin configuration

        Returns
        -------
        config_dict: dict
            additional key:value containing list of files and parsed timerange

        Raises
        ------
        ValueError:
            If no files were found
        """

        self.parse_date(config_dict)
        
        config_dict["variable"] = "pr"
        config_dict["time_frequency"] = "1day"
        search_keys = (
            f"variable",
            f"project",
            f"product",
            f"institute",
            f"model",
            f"experiment",
            f"time_frequency",
        )
        search_args = {key: config_dict[key] for key in search_keys}
        try:
            search_args["time"] = config_dict["seldate"]
        except:
            pass
        if (
            config_dict["ensemble"] != "*"
            and config_dict["ensemble"] is not None
        ):
            search_args["ensemble"] = config_dict["ensemble"]

        config_dict["files"] = list(freva.databrowser(**search_args))
        config_dict["ensemble"] = freva.facet_search(**search_args, facet=["ensemble"])[
            "ensemble"
        ]

        if not config_dict["files"]:
            raise ValueError(f"No files found for {search_args}")
        logger.debug("Found %i files", len(set(config_dict["files"])))

    # -----------

    def run_tool(self, config_dict=None):
        import json
        from pathlib import Path

        os.makedirs(config_dict["cache"], exist_ok=True)
        os.makedirs(config_dict["outputdir"], exist_ok=True)
        errorfile = Path(config_dict["cache"]) / "error.log"
        json_file = Path(config_dict["cache"]) / "out.json"            
        if not config_dict["inputdir"]:
            self.find_files(config_dict)
        else:
            config_dict["files"] = list(config_dict["inputdir"].split(","))
            self.parse_date(config_dict)

        with json_file.open("w") as json_f:
            json.dump(config_dict, json_f, indent=4)

        with errorfile.open("w") as error_f:
            if config_dict["select_index"] == "psi":
                self.call(
                    f"python {Path(__file__).parent / 'psi-main.py'} {json_file}",
                    stderr=error_f,
                )
            else:
                self.call(
                    f"{self.class_basedir}/wetdry-main.sh {json_file}",
                    check=False,
                    stderr=error_f,
                )

       # Link output data to the freva databrowser
        if config_dict["link2database"] is True:
            print(f"\n... Linking output to data-browser")
            self.add_output_to_databrowser(
                config_dict["outputdir"],
                project=config_dict.get("project", "default_project"),
                product=config_dict.get("product", "default_product"),
                model=config_dict.get("model", "default_model"),
                institute=config_dict.get("institute", "default_institute"),
                time_frequency="day",
            )            

        # delete cache or link to it in outputdir
        if config_dict["cacheclear"] is True:
            rmtree(config_dict["cache"])
        else:
            os.symlink(config_dict["cache"], f'{config_dict["outputdir"]}/cache')

        print(
            f'\n\n... Analysis finished:\n        {config_dict["cache"] =}\n        {config_dict["outputdir"] =}'
        )
        
        return self.prepare_output(config_dict["outputdir"])
