.. ClimdexCalc documentation master file, created by
   sphinx-quickstart on Thu Jun 30 14:49:10 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ExamplePlugin's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   exampleplugin

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
