"""
utils.py

This script does some common data handling and transformation

Developer:
    Etor E. Lucio-Eceiza (lucio-eceiza@dkrz.de) Module D/Software - ClimXtreme
version: v1.0.0
date: 2024.05.08
"""

import os, sys

import numpy as np
import xarray as xr
from datetime import datetime
import json

import dask

dask.config.set(scheduler="threads", num_workers=4)
dask.config.set(**{"array.slicing.split_large_chunks": False})


class DataHandler:
    """
    class to handle input data, apply spatio temporal mask, unit transform,
    apply latitudinal correction etc.
    """

    def __init__(self, config_dict):
        """

        Args:
            config_dict (dict): dictionary with the configuration

        Returns:
            None
        """
        self.config_dict = config_dict
        self.variables_to_filter = ("time", "lon", "lat", "rlon", "rlat", "x", "y")
        self.variables_to_choose = ("pr", "prc", "prl")
        self.ds = xr.Dataset()

    def _set_varname(self, ds):
        """
        Set the value of the 'varname' attribute based on the provided dataset.

        Parameters:
            ds (Dataset): The dataset from which to determine the value of 'varname'.

        Returns:
            None
        """
        if self.config_dict["variable"] is None:
            data_vars = list(ds.data_vars)
            try:
                varname = [
                    var for var in data_vars if var.startswith(self.variables_to_choose)
                ][0]
                print(f"variable_name read as [{varname}]")
                setattr(self, f"varname", varname)
            except IndexError as e:
                varname = [
                    var
                    for var in data_vars
                    if not var.startswith(self.variables_to_filter)
                ][0]
                print(
                    f"""
                    variable_name read as [{varname}],\
                    different from [{self.variables_to_choose}]!\n
                       """
                )
                setattr(self, f"varname", varname)
        else:
            setattr(self, f"varname", self.config_dict["variable"])

    def _get_time_freq(self, data_array):
        time_diffs = np.diff(
            np.array(data_array["time"]).astype("datetime64[s]").view("int64")
        )
        most_common_time_diff = np.bincount(time_diffs).argmax()
        frequency_mapping = {
            3600: "h",
            86400: "D",
            2629746: "M",  # Average month length in seconds (30.44 days)
            31556952: "Y",  # Average year length in seconds (365.25 days)
        }
        for seconds, freq_str in frequency_mapping.items():
            if most_common_time_diff // seconds <= 1:
                return freq_str

    def _seldate(self):
        """
        Sets the initial and end dates from `config_dict` dictionary.

        Parameters:
            self (object): The instance of the class.

        Returns:
            tuple: A tuple containing the `ini_date` and `end_date` values.
        """

        if self.config_dict["seldate"] is None:
            ini_date, end_date = [None] * 2
        else:
            ini_date, end_date = self.config_dict["seldate"].split("to")
            if np.datetime64(ini_date) >= np.datetime64(end_date):
                ini_date, end_date = end_date, ini_date
        self.ini_date, self.end_date = ini_date, end_date
        return ini_date, end_date

    def _select_months(self, ds, months) -> xr.Dataset:
        """
        Selects specific months from an xarray dataset based on the input criteria.

        Parameters
        ----------
        ds : xarray.Dataset
            The dataset to select months from.
        months : str or list of int

        Returns
        -------
        xarray.Dataset
            A new dataset that only contains data from the selected months.

        Raises
        ------
        ValueError
            If an invalid value is passed as the 'months' parameter.
        """
        if months == "all":
            # Select all months
            return ds
        elif months == "JJA":
            # Select June, July, August
            return ds.sel(time=ds["time.month"].isin([6, 7, 8]))
        elif months == "SON":
            # Select September, October, November
            return ds.sel(time=ds["time.month"].isin([9, 10, 11]))
        elif months == "DJF":
            # Select December, January, February
            return ds.sel(time=ds["time.month"].isin([12, 1, 2]))
        elif months == "MAM":
            # Select March, April, May
            return ds.sel(time=ds["time.month"].isin([3, 4, 5]))
        elif months != None:  # isinstance(months, list):
            # Select specific months by number
            mons = [int(x) for x in months.split(",")]
            return ds.sel(time=ds["time.month"].isin(mons))
        else:
            # Invalid input
            raise ValueError(f"Invalid month selection: {months}")

    def _latlon_box(self):
        """
        Set the latitude and longitude values based on the provided configuration.

        Parameters:
            self (object): The instance of the class.

        Returns:
            tuple: A tuple containing the minimum and maximum latitude and longitude values.
        """
        if self.config_dict["latlon"] is None or self.config_dict["latlon"] == "None":
            lat_min, lat_max, lon_min, lon_max = [None] * 4
        else:
            lat_min, lat_max, lon_min, lon_max = map(
                float, self.config_dict["latlon"].split(",")
            )
        self.lat_min, self.lat_max, self.lon_min, self.lon_max = (
            lat_min,
            lat_max,
            lon_min,
            lon_max,
        )
        return lat_min, lat_max, lon_min, lon_max

    def _check_latlon_var(self, ds):
        """
        A function to check and set longitude and latitude variables based on input dataset coordinates.
        Parameters:
            ds (xarray.Dataset): The input dataset to check for longitude and latitude coordinates.
        Returns:
            Tuple: A tuple containing the longitude and latitude variable names.
        """
        try:
            if all(coord in ds.coords for coord in ["rlon", "rlat"]):
                self.lon, self.lat = ["rlon", "rlat"]
                try:
                    self.lat_pole = ds.rotated_pole.attrs["grid_north_pole_latitude"]
                    self.lon_pole = ds.rotated_pole.attrs["grid_north_pole_longitude"]
                except:
                    self.lat_pole, self.lon_pole = 0, 0
            elif all(coord in ds.coords for coord in ["lon", "lat"]):
                self.lon, self.lat = ["lon", "lat"]
                self.lat_pole, self.lon_pole = 0, 0
        except:
            raise ValueError(f"Coordinates unknown in {ds.coords = }")
        return self.lat, self.lon

    def _mask_area(self, ds):
        """
        Masks the given dataset to the specified area.

        Parameters:
            ds (xr.Dataset): The dataset to mask.

        Returns:
            xr.Dataset: The masked dataset.
        """
        _, _, _, _ = self._latlon_box()
        if self.config_dict["latlon"]:
            if self.grid_type == "gaussian" or self.grid_type == "regular":
                return ds.sel(
                    lat=slice(self.lat_min, self.lat_max),
                    lon=slice(self.lon_min, self.lon_max),
                )
            elif self.grid_type == "curvilinear":
                mask_lon = (ds[self.lon] + self.lon_pole >= self.lon_min) & (
                    ds[self.lon] + self.lon_pole <= self.lon_max
                )
                mask_lat = (ds[self.lat] + self.lat_pole >= self.lat_min) & (
                    ds[self.lat] + self.lat_pole <= self.lat_max
                )
                mask = (mask_lon & mask_lat).compute()
                ds = ds.where(mask, drop=True)
                return ds
            elif self.grid_type == "rotated":
                self.ds["rotated_pole"] = ds["rotated_pole"]
                cyl = ccrs.PlateCarree()
                self.rotated_pole = ccrs.RotatedPole(
                    pole_latitude=self.lat_pole, pole_longitude=self.lon_pole
                )
                x1, y1 = self.rotated_pole.transform_point(
                    self.lon_min, self.lat_min, cyl
                )
                x2, y2 = self.rotated_pole.transform_point(
                    self.lon_max, self.lat_max, cyl
                )
                return ds.sel({self.lat: slice(y1, y2), self.lon: slice(x1, x2)})
        else:
            return ds

    def _resample(self, ds):
        """
        Resamples the given xarray Dataset to a daily frequency for subdaily time differences.

        Parameters:
            ds (xarray.Dataset): The Dataset to be resampled.

        Returns:
            ds (xarray.Dataset): The resampled Dataset.

        Raises:
            ValueError: for lower than daily frequencies.
        """
        time_diff = ds["time"][1] - ds["time"][0]
        is_days = np.timedelta64(1, "D") == time_diff
        if not is_days:
            if time_diff.item().total_seconds() / (3600 * 24) < 1:
                raise ValueError("Data frequency lower than daily.")
            print(
                f"Resampling from {time_diff.item().total_seconds() / 3600} hours to daily."
            )
            return (
                ds.resample(time="1D", skipna=False).mean(keep_attrs=True)
                if "kg m" in ds.units
                else ds.resample(time="1D", skipna=False).sum(keep_attrs=True)
            )
        else:
            return ds

    def _calc_area(self, ds):
        """method to calculate the area for each grid point"""
        pass

    def _standard_units(self, ds):
        """
        Convert the given dataset to standard units.

        Parameters:
            ds (xarray.Dataset): The dataset to be converted.

        Returns:
            xarray.DataArray: The converted precipitation datarray in standard units.
        """
        units = ds[self.varname].units
        conversion_factors = {
            "kg m-2 h-1": 24,
            "kg m^-2 h^-1": 24,
            "kg m-2 s-1": 86400,
            "kg m^-2 s^-1": 86400,
            "m/d": 1 / 1000,
        }
        if units != "mm/d":
            conversion_factor = conversion_factors.get(units)
            if conversion_factor is not None:
                ds[self.varname] = (ds[self.varname] * conversion_factor).astype(
                    "float32"
                )
                print(f"        data in [{units}]: convert to [mm/d]")
            elif units == "Kg m-2":
                print(
                    f"          data in [Kg m-2]: for the MiKlip ensemble representing accumulated precipitation"
                )
            else:
                raise ValueError(
                    f"{self.files} not in {list(conversion_factors.keys()) + ['mm/d']}: wrong units"
                )
        return ds[self.varname]

    def _get_coordinate_grid_type(self, ds):
        """
        Determines the type of coordinate grid based on the input dataset.

        Parameters:
            ds (xarray.Dataset): The input dataset containing latitude and longitude variables.

        Returns:
            str: The type of coordinate grid. Possible values are "gaussian", "regular", "curvilinear", or "unknown".
        """
        lat_var, _ = self._check_latlon_var(ds)
        lat = ds[lat_var]
        if lat.ndim == 1:
            if lat_var == "lat":
                if lat.attrs.get("bounds"):
                    return "gaussian"
                else:
                    return "regular"
            elif lat_var == "rlat":
                return "rotated"
        elif lat.ndim == 2:
            return "curvilinear"
        else:
            return "unknown"

    def _reshape_grid(self, ds):
        """
        Reshapes the given grid dataset.

        Parameters:
            ds (xarray.Dataset): The input grid dataset.

        Returns:
            xarray.Dataset: The reshaped grid dataset.
        """
        self._set_varname(ds)
        self.grid_type = self._get_coordinate_grid_type(ds)
        print(f"        the dataset is {self.grid_type = }")
        lon_name = (
            [index for index in ds.indexes if "lon" in index]
            or [coord for coord in ds.coords if "lon" in coord]
        )[0]
        if hasattr(ds, "time_bnds"):
            self.ds["time_bnds"] = ds["time_bnds"]
        if ds[lon_name].attrs["units"] == "degrees_east":
            input = {lon_name: (((ds[lon_name] + 180) % 360) - 180)}
            ds = ds.assign_coords(input)
            try:
                ds = ds.sortby(lon_name)
            except ValueError:
                is_curvilinear = len(ds["lon"].shape) == 2
                if is_curvilinear:
                    pass
                else:
                    raise ValueError
        return ds

    def _latitudinal_correction(self, da):
        """
        Apply latitudinal correction to the given dataset.

        This function applies a latitudinal correction to the given dataset based on the configuration
        parameters. It checks the value of the 'area_weight' parameter in the 'config_dict' attribute
        of the class instance and applies the corresponding correction to the dataset.

        Parameters:
            da (DataArray): The datarray to apply the correction to.

        Returns:
            DataArray: The datarray with the latitudinal correction applied.
        """
        if self.grid_type == "rotated":
            print(
                f"        Dataset with rotated poles: no latitudinal weighting applied"
            )
            return da
        if (
            not self.config_dict["area_weight"]
            or self.config_dict["area_weight"] == "None"
        ):
            print(f"        no latitudinal weighting applied")
            return da
        elif self.config_dict["area_weight"] == "cos":
            print(f"        latitudinal weighting: cos({self.lat})")
            weights = np.cos(np.deg2rad(da[self.lat]))
        elif self.config_dict["area_weight"] == "sqrt(cos)":
            print(f"        latitudinal weighting: sqrt( abs( cos({self.lat}) ) )")
            weights = np.sqrt(np.abs(np.cos(np.deg2rad(da[self.lat]))))
        da = (da * weights).astype("float32")
        return da

    def _transform_dataset(self, ds):
        """
        Transforms the given dataset by performing various operations on it.

        Args:
            ds (xarray.Dataset): The dataset to be transformed.

        Returns:
            xarray.Dataset: The transformed dataset.
        """
        ds = self._resample(ds)
        ds = self._select_months(ds, self.config_dict["selmon"])
        ds = self._reshape_grid(ds)
        ds = self._mask_area(ds)
        da = self._standard_units(ds)
        da = self._latitudinal_correction(da)
        return da

    def open_files(self, ensemble_member):
        """
        Open files for a given ensemble member.

        Args:
            ensemble_member (str): The name of the ensemble member.

        Returns:
            xr.DataArray: The precipitation dataArray containing the opened files.

        Raises:
            None.
        """
        print(f"\nreading data")
        if ensemble_member != "no-member":
            self.files = [
                file
                for file in list(self.config_dict["files"])
                if ensemble_member.lower() in file.lower()
            ]
        else:
            self.files = self.config_dict["files"]
        ini_date, end_date = self._seldate()

        try:
            ds = xr.open_mfdataset(
                self.files[:],
                combine="by_coords",
                parallel=True,
                chunks="auto",
                engine="netcdf4",
            ).sel(time=slice(ini_date, end_date))
            ds.unify_chunks()
        except NotImplementedError as e:
            ds = xr.open_mfdataset(
                self.files[:], combine="by_coords", parallel=True, engine="netcdf4"
            ).sel(time=slice(ini_date, end_date))
        if ini_date is None:
            self.ini_date = np.datetime_as_string(ds.time[0], unit="D")
            self.end_date = np.datetime_as_string(ds.time[-1], unit="D")
        da = self._transform_dataset(ds)
        return da
