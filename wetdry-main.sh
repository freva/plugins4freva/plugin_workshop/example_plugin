#!/bin/bash
set -e # DEBUGGING
# CONSECUTIVE WET AND DRY DATES


#####################################################################
# CONFIGURATION. LOAD FROM PYTHON WRAPPER:
#####################################################################
SCRIPT_PATH=$(dirname $0)
_jq(){
    jq -r ${1} ${2} | sed "s/null/None/g"
}

CACHE="$(_jq .cache $1)"                    # temporal folder
OUTPUTDIR="$(_jq .outputdir $1)"            # outputdir  (default: $USER_OUTPUT_DIR)
FILES="$(_jq .files[] $1)"                  # input files 
SELDATE="$(_jq .seldate $1)"                # seldate, YYYYMMDD,YYYYMMDD
LATLON="$(_jq .latlon $1)"                  # latlon, startlat,endlat,startlon,endlon
THRESHOLD="$(_jq .threshold $1)"            # threshold, in mm/day
CONSECUTIVE="$(_jq .persistence $1)"        # consecutive, in days
LINK2DATABASE="$(_jq .link2database $1)"    # link2database

#####################################################################
# MAIN PROGRAM
#e####################################################################

files=$(echo $FILES | sed 's/ /,/g')
IFS="," read -ra FILES <<< "$files"

################
# BASIC CHECKS:

# CHECKS THAT WE ARE INDEED LOADING PRECIPITATION FILES:
VAR=$(cdo -s showvar ${FILES[0]}  | awk '{print $1}')
if [[ $VAR != 'pr' ]]; then
    echo -e "\n    ERROR: must select PRECIPITATION variable (e.g. 'pr')"
    exit 1
fi

# CHECKS THAT WE ARE INDEED LOADING DAILY FILES:
TIMESTEP=$(cdo -s ntime ${FILES[0]}  )
DATES=$(cdo -s showdate ${FILES[0]}   | awk '{print NF}') # number of days
MONTHS=$(cdo -s showdate ${FILES[0]}   | awk '{for(i=1;i<=NF;i++){print substr($i,1,7)}}' | sort -u | wc -l) # number of diferent months
if [[ $TIMESTEP != $DATES || $TIMESTEP == $MONTHS ]]; then
    echo -e "\n    ERROR: must select DAILY timesteps"
    exit 1
fi

# CHECKS THAT THE UNITS ARE AMONG THE RECOGNISED ONES:
UNITS=$(cdo -s showunit ${FILES[0]} )
if [[ $UNITS == "kg m-2 s-1" || $UNITS == " kg m-2 s-1" || $UNITS == " kg m^-2 s^-1" ]]; then
    FACTOR=86400
    MESSAGE="Units will be converted from [kg m-2 s-1] to [mm/d]"
elif [[ $UNITS == " m/d" ]]; then
    FACTOR=0.001
    MESSAGE="Units will be converted from [m/d] to [mm/d]"
elif [[ $UNITS == " mm/d" || $UNITS == " kg m-2" ]]; then
    FACTOR=1
    MESSAGE="Units already in [mm/d], no conversion needed"
else
    echo -e "\n    ERROR: unknown/wrong unit ["$UNITS"]. Must select a PRECIPITATION variable in either [kg m-2 s-1], [kg m-2], [mm/d] or [m/d]" 
    exit 1
fi

###############
# CHECK FOR LATLON:

echo -e "\n... Checking geographic area  [$LATLON]"
if [[ -n "$LATLON" ]]; then

    IFS="," read -ra arr <<< "$LATLON"
    
    GRIDPOINT1=${arr[0]};
    GRIDPOINT2=${arr[1]};
    GRIDPOINT3=${arr[2]};
    GRIDPOINT4=${arr[3]};

else
    echo -e "\n    ERROR: Must select a geographic area: startlat,endlat,startlon,endlon" 
    exit 1
fi


################
# PREPROCESSING:
if [ ${#FILES[@]} -gt 1 ]; then
    INPUT=$CACHE/merge.nc
    cdo merge ${FILES[@]} $INPUT
else
    INPUT=${FILES[0]}
fi

dates=$(echo $SELDATE | sed 's/to/,/g')
if [[ "$SELDATE" != None  && -n "$SELDATE" ]]; then
    cdo -s -seldate,$dates $INPUT $CACHE/merge_tmp.nc
    INPUT=$CACHE/merge_tmp.nc
fi 




# LATITUDE INVERSION:
#
GRIDDES=$CACHE/griddes2.txt
cdo griddes2 $INPUT >$GRIDDES 
awk '{ if (/^yvals/) if ($3-$4>0) {print "Latitudes: north2south"} }' $GRIDDES >> $GRIDDES

if [[ -n $(grep -i north2south $GRIDDES) ]]; then
    cdo -s invertlat $INPUT $CACHE/merge_inv.nc 
    INPUT=$CACHE/merge_inv.nc
    echo -e "\n\tlatitude inversion needed."
fi

# ROTATED GRID:
#
if [ -n "$(cdo -s griddes $INPUT | grep rotated)" ]; then 
    GRIDDESROT=$CACHE/griddes_rot.txt
    cdo -s griddes $INPUT > $GRIDDESROT
    XINC=$(awk '{ if (/^xinc/) {print $3} } ' $GRIDDESROT)
    YINC=$(awk '{ if (/^yinc/) {print $3} } ' $GRIDDESROT)
    
    cdo -s -r -f nc copy -remapbil,r$(echo "360*(1/$XINC)" | bc -l | xargs printf "%1.0f" )x$(echo "180*(1/$YINC)" | bc -l | xargs printf "%1.0f" ) $INPUT $CACHE/merge_rot.nc 1>/dev/null 
    INPUT=$CACHE/merge_rot.nc
fi

# CURVILINEAL GRID:
#
if [ -n "$(cdo -s griddes $INPUT | grep curvilinear)" ]; then 
    echo REGRID
    GRIDDESCURV=$CACHE/griddes_curv.txt
    SINFOVCURV=$CACHE/sinfov.txt
    cdo -s griddes $INPUT > $GRIDDESCURV
    cdo -s sinfov $INPUT > $SINFOVCURV
    IFS=" " read lon dopp lonstart to lonend lonunit <<< "$(grep -F "lon : " $SINFOVCURV )"
    IFS=" " read lat dopp latstart to latend latunit <<< "$(grep -F "lat : " $SINFOVCURV)"
    IFS=" " read num dopp curvi dopp2 points sizeBrackets <<< "$(grep -F " curvilinear " $SINFOVCURV)"
    size=$(echo $sizeBrackets | rev | cut -c 2- | rev | cut -c 2-)
    IFS="x" read lonsize latsize <<< "$size"
    XINC=$(awk "BEGIN {printf \"%.2f\n\", ($lonstart - $lonend)/ $lonsize}" | sed 's/-//g')
    YINC=$(awk "BEGIN {printf \"%.2f\n\", ($latstart - $latend)/ $latsize}" | sed 's/-//g')
    XFIRST=$(awk "BEGIN {printf \"%.2f\n\", $lonstart}")
    YFIRST=$(awk "BEGIN {printf \"%.2f\n\", $latstart}")
    GRIDSIZE=$(awk "BEGIN {printf \"%.1i\n\", $lonsize * $latsize}")

cat <<EOF >  $CACHE/new_grid.txt
####
# gridID 25
#
gridtype  = lonlat
gridsize  = $GRIDSIZE
xname     = lon
xlongname = Longitude values
xunits    = degrees_east
yname     = lat
ylongname = Latitude values
yunits    = degrees_north
xsize     = $lonsize
ysize     = $latsize
xfirst    = $XFIRST
xinc      = $XINC
yfirst    = $YFIRST
yinc      = $YINC
EOF

    cdo -s -r -f nc copy -remapbil,$CACHE/new_grid.txt $INPUT $CACHE/merge_cur.nc 1>/dev/null 
    INPUT=$CACHE/merge_cur.nc
fi

# SELECTING GEOGRAPHICAL AREA
#
cdo -s -sellonlatbox,$GRIDPOINT3,$GRIDPOINT4,$GRIDPOINT1,$GRIDPOINT2 $INPUT $CACHE/merge_box.nc
INPUT=$CACHE/merge_box.nc


################
# CONVERTING THE FILE FROM ARBITRARY UNITS TO [mm/d]:
#
echo -e "\n... Unit conversion"
echo -e "\n\t$MESSAGE"
if [[ $FACTOR != 1 ]]; then
    cdo -s mulc,$FACTOR $INPUT $CACHE/merge_units.nc
    INPUT=$CACHE/merge_units.nc
fi

###############
# CALCULATION OF THE INDEXES:
#
echo -e "\n... Calculation of Wet and Dry Consecutive Days"
dates=$(echo $dates | sed 's/-//g;s/,/-/g')
WET=wetdays-${dates}
DRY=drydays-${dates}
cdo -s eca_cwd,$THRESHOLD,$CONSECUTIVE $INPUT $OUTPUTDIR/$WET".nc"
cdo -s eca_cdd,$THRESHOLD,$CONSECUTIVE $INPUT $OUTPUTDIR/$DRY".nc"

################
# REMOVING VARIABLE SO THE FILES CAN BE CRAWLED (1 VAR x FILE):
#
if [ $LINK2DATABASE == "true" ]; then
    cdo -delname,number_of_cdd_periods_with_more_than_2days_per_time_period $OUTPUTDIR/$DRY".nc" $OUTPUTDIR/TMP.nc
    cdo -chname,consecutive_dry_days_index_per_time_period,cdd $OUTPUTDIR/TMP.nc $OUTPUTDIR/$DRY".nc"
    cdo -delname,number_of_cwd_periods_with_more_than_2days_per_time_period $OUTPUTDIR/$WET".nc" $OUTPUTDIR/TMP.nc
    cdo -chname,consecutive_wet_days_index_per_time_period,cwd $OUTPUTDIR/TMP.nc $OUTPUTDIR/$WET".nc"
    rm $OUTPUTDIR/TMP.nc
fi

####################
# PLOTTING:
#
echo -e "\n... Plotting"

python $SCRIPT_PATH/wetdry_plotter.py $OUTPUTDIR/$WET".nc" $OUTPUTDIR/$DRY".nc" $OUTPUTDIR/$WET".png" $OUTPUTDIR/$DRY".png" 


