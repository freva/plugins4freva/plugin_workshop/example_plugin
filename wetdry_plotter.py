import sys
import numpy as np
import xarray as xr
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeature

wetDaysFilename = sys.argv[1]  ## wetdays filename
dryDaysFilename = sys.argv[2]  ## drydays filename

wetDaysDataset = xr.open_dataset(wetDaysFilename)
dryDaysDataset = xr.open_dataset(dryDaysFilename)

wetDays = wetDaysDataset.consecutive_wet_days_index_per_time_period.isel(time=0).isel(sfc=0)
dryDays = dryDaysDataset.consecutive_dry_days_index_per_time_period.isel(time=0).isel(sfc=0)

lon_min = dryDays.lon.min().item()
lon_max = dryDays.lon.max().item()
lat_min = dryDays.lat.min().item()
lat_max = dryDays.lat.max().item()

fig = plt.figure(figsize=(12, 8))
ax = plt.axes(projection=ccrs.PlateCarree())
ax.set_extent([lon_min, lon_max, lat_min, lat_max], crs=ccrs.PlateCarree())
ax.add_feature(cfeature.BORDERS, linestyle=':')
ax.add_feature(cfeature.COASTLINE)
ax.add_feature(cfeature.OCEAN, zorder=1, facecolor='lightblue')
ax.add_feature(cfeature.LAND, zorder=0, edgecolor='black')
ocean = cfeature.NaturalEarthFeature(
    "physical", "ocean", "50m", facecolor="white"
)
ax.add_feature(ocean)
masked_data = np.ma.masked_where(np.isnan(dryDays), dryDays)
contour = ax.contourf(dryDays.lon, dryDays.lat, masked_data, transform=ccrs.PlateCarree(), cmap='viridis')
cb = plt.colorbar(contour, ax=ax, orientation='horizontal', pad=0.05, shrink=0.5)
cb.set_label('Consecutive Dry Days')
plt.title('Consecutive Dry Days')
plt.savefig(sys.argv[3], bbox_inches='tight', dpi=100)

fig = plt.figure(figsize=(12, 8))
ax = plt.axes(projection=ccrs.PlateCarree())
ax.set_extent([lon_min, lon_max, lat_min, lat_max], crs=ccrs.PlateCarree())
ax.add_feature(cfeature.BORDERS, linestyle=':')
ax.add_feature(cfeature.COASTLINE)
ax.add_feature(cfeature.OCEAN, zorder=1, facecolor='lightblue')
ax.add_feature(cfeature.LAND, zorder=0, edgecolor='black')
ocean = cfeature.NaturalEarthFeature(
    "physical", "ocean", "50m", facecolor="white"
)
ax.add_feature(ocean)
masked_data = np.ma.masked_where(np.isnan(wetDays), wetDays)
contour = ax.contourf(wetDays.lon, wetDays.lat, masked_data, transform=ccrs.PlateCarree(), cmap='viridis')
cb = plt.colorbar(contour, ax=ax, orientation='horizontal', pad=0.05, shrink=0.5)
cb.set_label('Consecutive Wet Days')
plt.title('Consecutive Wet Days')
plt.savefig(sys.argv[4], bbox_inches='tight', dpi=100)
