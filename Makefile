export PATH := $(PWD)/plugin_env/bin:$(PATH)
.PHONY: docs
all: conda cartopy

conda:
	@# This installs a conda environment
	rm -rf ./plugin_env
	mamba env create --prefix ./plugin_env -f plugin-env.yml

cartopy:
	@# This downloads various cartopy shape-files
	rm -rf ./cartopy
	./plugin_env/bin/python3 assets/cartopy_download.py physical cultural

clean:
	@# This removes the conda environment
	rm -rf ./plugin_env
