---
title: The Example Plugin
---

**authors:**

> Freva Team (DKRZ. Dpt Data Analysis, CLINT)[^1]
>
> | Deutsches Klimarechenzentrum (DKRZ)
>
> | Version from 2024-03-18
> | Repository:
>   <https://gitlab.dkrz.de/freva/plugins4freva/plugin_workshop/example_plugin/>

This plugin calculates three indices:

:   -   the Consecutive Dry and Wet days (CDD and CWD).
    -   the Precipitation Severity Index (PSI). The PSI is used to
        detect the extreme events including three different, but
        intertwined aspects of extreme precipitation: grid-point
        intensity, spatial extent of affected area and time persistence.

The analysis is done according to certain defined daily precipitation
threshold. Persistence is also considered.

# Theoretical Background for CDD

[(from the CDO
documentation)](https://earth.bsc.es/gitlab/ces/cdo/raw/b4f0edf2d5c87630ed4c5ddee5a4992e3e08b06a/doc/cdo_eca.pdf)

Let ifile be a time series of the daily precipitation amount RR, then
the largest number of consecutive days where RR is **less** than R is
counted. - R is an optional parameter with default R = 1 mm. - A further
output variable is the number of dry periods of more than 5 days. The
date information of a timestep in ofile is the date of the last
contributing timestep in ifile.

# Theoretical Background for CWD

[(from the CDO
documentation)](https://earth.bsc.es/gitlab/ces/cdo/raw/b4f0edf2d5c87630ed4c5ddee5a4992e3e08b06a/doc/cdo_eca.pdf)

Let ifile be a time series of the daily precipitation amount RR, then
the largest number of consecutive days where RR is **greater or equal**
than R is counted. - R is an optional parameter with default R = 1 mm. -
A further output variable is the number of dry periods of more than 5
days. The date information of a timestep in ofile is the date of the
last contributing timestep in ifile.

# Theoretical Background for PSI

PSI is a unitless index that indicates the degree of daily precipitation
severity with respect to a predetermined climatological threshold. Large
PSI values represent high intensity, geographically extensive and
temporally persistent precipitation events.

The PSI is adapted from the *Storm Severity Index* (SSI; Leckebusch et
al., 2008)[^2]; Pinto et al., 2012[^3]) and further developments by
Piper et al., (2016)[^4]. It is defined as follows:

$$PSI_T=\frac{1}{(1+d)A}\sum_{i=1}^{N}\sum_{j=1}^{M}\sum_{t=T-d}^{T}\frac{RR_{ijt}}{RR_{perc_{ij}}}\cdot(\Delta x)^2\cdot\prod_{\tau=t}^{T}I(RR_{ij\tau},RR_{perc_{ij}})$$

where

$$\begin{aligned}
I(RR_{ij\tau},RR_{perc_{ij}})=\left\{\begin{matrix}0\;if\;RR_{ij\tau}\leq RR_{perc_{ij}}\\1\;if\;RR_{ij\tau}\ge RR_{perc_{ij}}\end{matrix}\right.
\end{aligned}$$

The PSI values at a certain time step T ($PSI_{T}$) depends on the ratio
between grid point daily precipitation ($RR_{ijt}$) and a percentile of
the climatology ($RR_{{perc}_{ij}}$). The $n^{th}$ percentile threshold
ensures that only precipitation events with high grid-point intensity
(above said threshold) are considered. The grid points whose
precipitation is lower than the set threshold for day T
($RR_{ij\tau}\leq RR_{perc_{ij}}$), by means of the function
$I(RR_{ij\tau},RR_{perc_{ij}})$ are, thus, neglected.

The precipitation persistence is considered through the sum over time
(𝑡). The ratios at each grid point for day T and the previous 𝑑 days are
added for the PSI calculation, provided precipitation was continuous and
larger than $RR_{{perc}_{ij}}$ at that same grid point 𝑖,𝑗.

To consider the size of each grid cell we multiply by the area of one
grid cell (∆𝑥)2. Finally, the ratios are summed over the spatial extent
(𝑁𝑥𝑀) along directions 𝑖 and j. The daily PSI value is normalized to the
area of the analysed domain 𝐴 = 𝑁 ∙ 𝑀 ∙ (∆𝑥)2 multiplied by (1+ 𝑑) to
consider the addition of grid points with persistent precipitation.

::: {.note}
::: {.title}
Note
:::

Prior to the PSI calculation, a correction for latitude stretching of
the grid as 𝑠𝑞𝑟𝑡(𝑐𝑜𝑠(𝑙𝑎𝑡)) following (North et al., 1982)[^5] is
included, but is optional.

This correction is not applied (regardless of the choice) for rotated
curvilinear grids, as these have been computed locating the poles the
furthest possible to minimise latitudinal effects.
:::

# Input

-   input (either/or):
    -   `inputdir`: entered by the user
    -   `SolrField`:
        -   `variable`: implicit, limited to precipitation (`pr`)
        -   `time_freq`: implicit, limited to daily data
        -   `ensemble`: multiple ensemble selection allowed
-   `outputdir`
-   `cachedir` + `cache clear`
-   `seldate`: analysis period
-   `latlon`: area selection
-   `threshold`: absolute values off mm/d
-   `persistence`: number of days
-   `select_index`: option between calculating `psi` or `cdd_cwd`
-   `link2database`: option to link the data back to the freva
    databrowser.

# Output

-   3 `netcdf` files, depending on the `select_index` option
    -   PSI index (`psi`)
    -   CWD (`cdd_cwd`)
    -   CDD (`cdd_cwd`)
-   plots:
    -   temporal series, PSI + 10 most extreme dates. (`psi`)
    -   spatial plot with delimited area (`psi`)
    -   consecutive dry days (`cdd_cwd`)
    -   consecutive wet days (`cdd_cwd`)

# Installation

All the needed libraries and dependencies of the plugin are installed
within a conda environment via `make` and a `yaml` file. This conda
environment is automatically activated by Freva at the time of the
plugin execution. To create the conda environment:

1.  load `conda`, for example by loading the Freva module, e.g in DKRZ:

``` {.bash}
module load clint <freva-instance> # for example
```

2.  create conda environment & download cartopy shapefiles:

``` {.bash}
cd /path/to/example_plugin/
make all
```

You can activate the conda environment at anytime via:

``` {.bash}
source /path/to/example_plugin/plugin_env/bin/activate
```

# Updates

\--

# How to cite this plugin

\--

# References

[^1]: <freva@dkrz.de>

[^2]: Leckebusch, G. C., Renggli, D., and Ulbrich, U.: Development and
    application of an objective storm severity measure for the Northeast
    Atlantic region, Meteorol. Z., 17, 575--587,
    [https://doi.org/10.1127/0941-2948/2008/0323](https://www.schweizerbart.de/papers/metz/detail/17/56773/Development_and_application_of_an_objective_storm_?af=crossref),
    2008.

[^3]: Pinto, J. G., Karremann, M. K., Born, K., Della-Marta, P. M., and
    Klawa, M.: Loss potentials associated with European windstorms under
    future climate conditions, Clim. Res., 54, 1--20,
    [https://doi.org/10.3354/cr01111](https://www.int-res.com/abstracts/cr/v54/n1/p1-20/),
    2012.

[^4]: Piper, D., Kunz, M., Ehmele, F., Mohr, S., Mühr, B., Kron, A., and
    Daniell, J.: Exceptional sequence of severe thunderstorms and
    related flash floods in May and June 2016 in Germany -- Part 1:
    Meteorological background, Nat. Hazards Earth Syst. Sci., 16,
    2835--2850, <https://doi.org/10.5194/nhess-16-2835-2016>, 2016.

[^5]: North, G. R., Moeng, F. J., Bell, T. L., and Cahalan, R. F.: The
    Latitude Dependence of the Variance of Zonally Averaged Quantities,
    Monthly Weather Review, 110, 319--326,
    <https://doi.org/10.1175/1520-0493(1982)110%3C0319:TLDOTV%3E2.0.CO;2>,
    1982.
